$(document).ready(function() {

	$('#cart-l').click(function() {
		if($('#cart').hasClass('hidden')) {
			$('#cart').removeClass('hidden').animate({'right':'0px'},300);
		} else {
			$('#cart').addClass('hidden').animate({'right':'-200px'},300);
		}
	});

	$('#menu-t ul li').mouseover(function(){
		var li = $(this);
		$(this).append('<div class="menu-t-h"></div>');
		$('.menu-t-h').css('width',li.width());
	});
	$('#menu-t ul li').mouseout(function(){
		$('.menu-t-h').remove();
	});

	$('.c-n').each(function() {
		var w = $(this).width();
		$(this).css('margin-left',-(w/2)+'px');
	});

	$("#rslider").ionRangeSlider({
		 postfix: " кг",
		 step: 0.5,
	});

	$('.elem-block img').hover(function() {
		if(!$(this).next('.elem-desc').hasClass('show')) {
			$(this).next('.elem-desc').addClass('show');
		}

	});
	$('.elem-block img').mouseleave(function() {
		$(this).next('.elem-desc').removeClass('show');
	});

	$('.elem-block .b').click(function() {
		$('.modal.flower').fadeIn();
		return false;
	});
	$('.add-comment').click(function() {
		$('.modal.cakeform-comment').fadeIn();
		return false;
	});
	$('#wishes .b').click(function() {
		$('.modal.calc').fadeIn();
		return false;
	});
	$('.modal .close').click(function() {
		$('.modal').fadeOut();
	});

	$('.radio').click(function() {
		$('.radio').each(function() {
			if($(this).hasClass('checked')) {
				$(this).removeClass('checked');
				$(this).next('input:radio').prop('checked',false);
			}
		});
		$(this).addClass('checked');
		$(this).next('input:radio').prop('checked',true);
	});

	$('.checkbox').click(function() { 
		if($(this).hasClass('checked')) {
			$(this).removeClass('checked');
			$(this).next('input:checkbox').prop('checked',false);
		} else {
			$(this).addClass('checked');
			$(this).next('input:checkbox').prop('checked',true);
		}
	});

	$('.c').dotdotdot();

	//$('.form-upload').upload();

});